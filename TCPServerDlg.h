
// TCPServerDlg.h : header file
//

#pragma once


// CTCPServerDlg dialog
class CTCPServerDlg : public CDialogEx
{
// Construction
public:
	CTCPServerDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_TCPSERVER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonStart();
private:
	HANDLE m_hThread;
public:
	CEdit m_EditEvent;
	CEdit m_Port;
};
