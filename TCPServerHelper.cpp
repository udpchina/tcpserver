
// TCPServerHelper.cpp : implementation file
//

#include "stdafx.h"
#include "TCPServerHelper.h"
#include "CSocket2Api.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


/*
	global variable define
*/
WORD	gwListenPort = 9000;
#define RCVBUFFSIZE (1024*64)
CEdit* gpEditEvent = NULL;

// CTCPServerDlg dialog



DWORD WINAPI TCPServer(LPVOID arg)
{
	WSADATA wsa;
	int retval=0;

	// variable to use data comm.
	SOCKET client_sock;
	SOCKADDR_IN clientaddr;
	int addrlen;
	HANDLE hThread;
	DWORD ThreadId;

	// winsock start
	if(WSAStartup(MAKEWORD(2,2), &wsa) != 0)
	{
		return 0;
	}

	// socket()
	SOCKET listen_sock = socket(AF_INET, SOCK_STREAM, 0);
	if(listen_sock == INVALID_SOCKET)
	{
		AfxMessageBox( "[TCP Server] socket() error" );
		return 0;
	}

	// find IP
	char name[255];
	char *ip;
	PHOSTENT hostinfo;
	if (gethostname(name, sizeof(name)) == 0)
	{
		if((hostinfo = gethostbyname(name)) != NULL)
		{
			ip = inet_ntoa(*(struct in_addr *)*hostinfo->h_addr_list);
		}
	}

	// bind()
	SOCKADDR_IN serveraddr;
	ZeroMemory(&serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_port = htons((int)gwListenPort);//htons(9000);
	serveraddr.sin_addr.s_addr = inet_addr(ip);//inet_addr("192.168.2.199");//htonl(INADDR_ANY);
	retval = bind(listen_sock, (SOCKADDR *)&serveraddr, sizeof(serveraddr));
	if(retval == SOCKET_ERROR)
	{
		AfxMessageBox( "[TCP Server] bind error" );
		return 0;
	}

	// listen
	retval = listen(listen_sock, SOMAXCONN);
	if(retval == SOCKET_ERROR)
	{
		AfxMessageBox( "[TCP Server] listen error" );
		return 0;
	}

	while(1)
	{
		// accept()
		addrlen = sizeof(clientaddr);
		client_sock = accept(listen_sock,
			(SOCKADDR *) &clientaddr, &addrlen);

		if(client_sock == INVALID_SOCKET)
		{
			AfxMessageBox( "[TCP Server] accept() error" );
			continue;
		}

		hThread = CreateThread(NULL, 0, ProcessClient, (LPVOID)client_sock, 0, &ThreadId);
		if(hThread == NULL)
			AfxMessageBox( "[TCP Server] Client thread create error" );
		else
			CloseHandle(hThread);

	}

	// closesocket()
	closesocket(listen_sock);
	AfxMessageBox( "[TCP Server] Closed" );
	// winsock clean
	WSACleanup();
	return 0;
}

DWORD WINAPI ProcessClient(LPVOID arg)
{
	SOCKET client_sock = (SOCKET)arg;
	char strDatabuf[RCVBUFFSIZE];
	SOCKADDR_IN clientaddr;
	int addrlen;
	int retval;

	// Get Client information
	addrlen = sizeof(clientaddr);
	getpeername(client_sock, (SOCKADDR *)&clientaddr, &addrlen);

	while(1)
	{
		// Rcv Data (HEADER)
		retval = recv(client_sock, strDatabuf, 16, 0);//RCVBUFFSIZE, 0);

		if(retval == SOCKET_ERROR)
		{
			//MessageBox( ghwnd, "[TCP Server] recv error", "INFORMATION", MB_OK );
			break;
		}
		else if(retval == 0)
		{
			break;
		}

		if ( 0 != memcmp("DOOFTEN", strDatabuf, 7) )
		{
			continue;
		}

		// Rcv Data (DATA)
		retval = recv(client_sock, strDatabuf, RCVBUFFSIZE, 0);

		if(retval == SOCKET_ERROR)
		{
			//MessageBox( ghwnd, "[TCP Server] recv error", "INFORMATION", MB_OK );
			break;
		}
		else if(retval == 0)
		{
			break;
		}

		TRACE("\n===========\nTCP Push : %s\n===========\n", strDatabuf);

		// init before use.

		do 
		{				
			if( 0 != memcmp( "EVENT/1.0\n", strDatabuf, 10 ) ) break;
			gpEditEvent->LineScroll(gpEditEvent->GetLineCount());
			int len = gpEditEvent->GetWindowTextLength();
			gpEditEvent->SetSel(len, -1, true);
			gpEditEvent->ReplaceSel((TCHAR*)strDatabuf);
			
			break; // for exit...
		}while(1);

	}

	// closesocket()
	closesocket(client_sock);

	return 0;
}