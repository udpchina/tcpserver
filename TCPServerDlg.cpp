
// TCPServerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "TCPServer.h"
#include "TCPServerDlg.h"
#include "afxdialogex.h"
#include "CSocket2Api.h"
#include "TCPServerHelper.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CTCPServerDlg dialog




CTCPServerDlg::CTCPServerDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CTCPServerDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTCPServerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_EVENT, m_EditEvent);
	DDX_Control(pDX, IDC_EDIT_PORT, m_Port);
}

BEGIN_MESSAGE_MAP(CTCPServerDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_START, &CTCPServerDlg::OnBnClickedButtonStart)
END_MESSAGE_MAP()


// CTCPServerDlg message handlers

BOOL CTCPServerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	m_hThread = NULL;
	m_Port.SetWindowText("9000");
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CTCPServerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CTCPServerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CTCPServerDlg::OnBnClickedButtonStart()
{
	// TODO: Add your control notification handler code here
	gpEditEvent = (CEdit*) &m_EditEvent;
	
	CString szPort;
	m_Port.GetWindowText(szPort);
	szPort.ReleaseBuffer();

	gwListenPort = atoi( szPort.GetBuffer(szPort.GetLength()) );

	InitSocket();
	// TCP Server Init.
	DWORD ThreadId;
	m_hThread = CreateThread(NULL, 0, TCPServer, (LPVOID)NULL, 0, &ThreadId);

	if(m_hThread == NULL)
	{
		AfxMessageBox( "[TCP Server] Open Error" );
	}
	else
	{
		CloseHandle(m_hThread);
	}
	((CButton*)GetDlgItem(IDC_BUTTON_START))->SetWindowText("Running");
	((CButton*)GetDlgItem(IDC_BUTTON_START))->EnableWindow(FALSE);
}
